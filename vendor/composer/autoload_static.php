<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInita4a704029f99b3f8f98219f98bc57bd2
{
    public static $prefixLengthsPsr4 = array (
        'S' => 
        array (
            'Symfony\\Component\\EventDispatcher\\' => 34,
        ),
        'C' => 
        array (
            'Composer\\Installers\\' => 20,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Symfony\\Component\\EventDispatcher\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/event-dispatcher',
        ),
        'Composer\\Installers\\' => 
        array (
            0 => __DIR__ . '/..' . '/composer/installers/src/Composer/Installers',
        ),
    );

    public static $prefixesPsr0 = array (
        'T' => 
        array (
            'Tumblr\\API' => 
            array (
                0 => __DIR__ . '/..' . '/tumblr/tumblr/lib',
            ),
        ),
        'G' => 
        array (
            'Guzzle\\Tests' => 
            array (
                0 => __DIR__ . '/..' . '/guzzle/guzzle/tests',
            ),
            'Guzzle' => 
            array (
                0 => __DIR__ . '/..' . '/guzzle/guzzle/src',
            ),
        ),
        'E' => 
        array (
            'Eher\\OAuth' => 
            array (
                0 => __DIR__ . '/..' . '/eher/oauth/src',
            ),
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInita4a704029f99b3f8f98219f98bc57bd2::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInita4a704029f99b3f8f98219f98bc57bd2::$prefixDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInita4a704029f99b3f8f98219f98bc57bd2::$prefixesPsr0;

        }, null, ClassLoader::class);
    }
}
