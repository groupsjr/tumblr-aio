<?php

namespace sjr\tumblr_aio;

/**
*	set up autoloading for composer libs in /vendor/
*	@param string
*	@return
*/
require_once __DIR__.'/vendor/autoload.php';

/**
*	first step in connecting new account, takes consumer key and secret key and redirects 
*	to tumblr to prompt for app authorization
*	@param string
*	@param string
*/
function connect_tumblr( $consumer = '', $secret = '' ){
	$tumblr = new \Tumblr\API\Client( $consumer, $secret );

	$request_handler = $tumblr->getRequestHandler();
	$request_handler->setBaseUrl('https://www.tumblr.com/');

	// get the oauth_token
	$resp = $request_handler->request( 'POST', 'oauth/request_token', array(
		'oauth_callback' => admin_url( sprintf('admin.php?page=sjr-tumblr-accounts&sjr-tumblr-connect=1', $consumer, $secret) )
	) );
	parse_str( $resp->body, $data );

	$tumblr->setToken( $data['oauth_token'], $data['oauth_token_secret'] );

	$secure = 'https' === parse_url( site_url(), PHP_URL_SCHEME );
	$cookie = (object) array(
		'oauth_token' => $data['oauth_token'],
		'oauth_token_secret' => $data['oauth_token_secret'],
		'consumer_key' => $consumer,
		'consumer_secret' => $secret
	);
	setcookie( 'sjr-tumblr-connect', json_encode($cookie), time() + YEAR_IN_SECONDS, SITECOOKIEPATH, NULL, $secure );

	$url = sprintf( 'https://www.tumblr.com/oauth/authorize?oauth_token=%s', $data['oauth_token'] );
	wp_redirect( $url );

	die();
}

/**
*
*	@return string username of connected blog
*/
function connect_tumblr_do(){
	$data = json_decode( stripslashes($_COOKIE['sjr-tumblr-connect']) );

	return connect_tumblr_complete( $data->consumer_key, $data->consumer_secret, 
									$data->oauth_token, $data->oauth_token_secret, 
									$_GET['oauth_verifier'] );
}

/**
*
*	@param string
*	@param string
*	@param string
*	@param string
*	@param string
*	@return string username of connected blog
*/
function connect_tumblr_complete( $consumer = '', $secret = '', $oauth_token = '', $oauth_token_secret = '', $oauth_verifier = '' ){
	
	$tumblr = new \Tumblr\API\Client( $consumer, $secret, $oauth_token, $oauth_token_secret );

	$request_handler = $tumblr->getRequestHandler();
	$request_handler->setBaseUrl( 'https://www.tumblr.com/' );
	
	$resp = $request_handler->request( 'POST', 'oauth/access_token', array(
		'oauth_verifier' => $oauth_verifier
	) );

	parse_str( $resp->body, $data );

	if( !empty($data['oauth_token']) && !empty($data['oauth_token_secret']) ){
		$settings = get_tumblr_accounts();

		$new_account = array(
			'consumer_key' => $consumer,
			'consumer_secret' => $secret,
			'access_token' => $data['oauth_token'],
			'access_token_secret' => $data['oauth_token_secret'],
		);

		try{
			$tumblr = new \Tumblr\API\Client( $new_account['consumer_key'], $new_account['consumer_secret'], $new_account['access_token'], $new_account['access_token_secret'] );
			$res = $tumblr->getUserInfo();	

			$settings[$res->user->name] = $new_account;
			update_option( 'sjr-tumblr-accounts', $settings, FALSE );

			return $res->user->name;

		} catch( \Tumblr\API\RequestException $e ){
		
		}
		
	} else {
		
	}
} 	

/**
*	Extracts a given string from another string according to a regular expression.
*	@TODO Add error handling for when the $post_body doesn't give us what we need to fulfill the Tumblr post type req's.
*	@param string $pattern The PCRE-compatible regular expression.
*	@param string $str The source from which to extract text matching the $pattern.
*	@param int $group If the regex uses capture groups, the number of the capture group to return.
*	@return string The matched text.
*/
function extract_by_regex( $pattern, $str, $group = 0 ){
	$matches = array();
	preg_match( $pattern, $str, $matches );
	
	return isset( $matches[$group] ) ? $matches[$group] : '';
}

/**
*	array map callback
*	@param WP_Post
*	@return string absolute path to attachment file
*/
function get_file_paths( $attachment ){
	$path = get_attached_file( $attachment->ID );

	return $path;
}

/**
*
*	@param string
*	@return array
*/
function get_setting( $option = '' ){
	$value = (array) get_option( $option );

	$default = array(
		'sjr-tumblr-settings' => array(
			'default' => '',
			'taxonomy' => 'tag',
		)
	);

	$value = array_merge( $default[$option], $value );
	return $value;
}

/**
*
*	@param string
*	@return Tumblr\API\Client
*/
function get_tumblr( $account_name = '' ){
	static $tumblrs = array();

	if( !isset($tumblrs[$account_name]) ){
		$account = get_tumblr_account( $account_name );

		if( $account['consumer_key'] && $account['consumer_secret'] ){
			$tumblr = new \Tumblr\API\Client( $account['consumer_key'], $account['consumer_secret'] );
		} else {
			//die( 'consumer key or secret not found' );
			$tumblr = FALSE;
		}

		if( $account['access_token'] && $account['access_token_secret'] ){
			$tumblr->setToken( $account['access_token'], $account['access_token_secret'] );
		}

		$tumblrs[$account_name] = $tumblr;
	}

	return $tumblrs[$account_name];
} 

/**
*	gets one tumblr account
*	@param string
*	@return array
*/
function get_tumblr_account( $account_name ){
	$accounts = get_tumblr_accounts();
	
	if( !isset($accounts[$account_name]) ){
		$accounts[$account_name] = array();
	}
	
	$account =  array_merge( array(
		'consumer_key' => '',
		'consumer_secret' => '',
		'access_token' => '',
		'access_token_secret' => '',
	), $accounts[$account_name] );

	return $account;
}

/**
*
*	@param string
*	@return string | FALSE
*/
function get_tumblr_account_name( $blog_name ){
	$accounts_blogs = get_tumblr_blogs();
	$account_name = FALSE;

	foreach( $accounts_blogs as $accounts_blog ){
		foreach( $accounts_blog->user->blogs as $blog ){
			if( $blog->name == $blog_name ){
				$account_name = $accounts_blog->user->name;
				break 2;
			}
		}
	}

	return $account_name;
}

/**
*	gets all connected tumblr accounts
*	@return array
*/
function get_tumblr_accounts(){
	$accounts = (array) get_option( 'sjr-tumblr-accounts' );
	

	return array_filter( $accounts );
}

/**
*
*	@return array
*/
function get_tumblr_blogs(){
	$accounts = get_tumblr_accounts();

	$accounts_blogs = array();

	foreach( $accounts as $blog_name => $creds ){
		$transient_key = substr( 'sjr-tubmlr-blog-'.$blog_name, 0, 40 );
		$res = get_transient( $transient_key );

		if( $res === FALSE ){
			$tumblr = get_tumblr( $blog_name );

			$res = $tumblr->getUserInfo();

			set_transient( $transient_key, $res, HOUR_IN_SECONDS );
		}

		$accounts_blogs[] = $res;
	}

	return $accounts_blogs;	
}

/**
*
*	@param int
*	@return array
*/
function prepare_for_tumblr( $post_id ){
	$prepared_post = (object) array();

	$format = get_post_format( $post_id );
	$state = wp_status_to_tumblr_state( get_post_status($post_id) );
	$tags = array();
	if( $t = get_the_tags($post_id) ){
		foreach( $t as $tag ){
			// Decode manually so that's the ONLY decoded entity.
			$tags[] = str_replace('&amp;', '&', $tag->name);
		}
	}

	// params for all content types
	$common_params = array(
		'type' => wp_post_format_to_tumblr_post_type($format),
		'state' => $state,
		'tags' => implode(',', $tags),
		'date' => get_post_time('Y-m-d H:i:s', TRUE, $post_id) . ' GMT',
		'format' => 'html', // Tumblr's "formats" are always either 'html' or 'markdown'
		'slug' => get_post_field('post_name', $post_id),
	);

	// params for specific content type
	$post_params = wp_post_to_tubmlr_params( $post_id, $common_params['type'] );

	$prepared_post = array_merge( $common_params, $post_params );

	return $prepared_post;
}

/**
*	render a page into wherever
*	(only used in admin screen)
*	@param string
*	@param object|array
*	@return string html
*/
function render( $filename, $vars = array() ){
	$template = __DIR__.'/views/'.$filename.'.php';
	ob_start();
	if( file_exists($template) ){
		extract( (array) $vars, EXTR_SKIP );
		include $template;
	}

	$html = ob_get_clean();
	return $html;
}

/**
*	Modified from https://stackoverflow.com/a/4997018/2736587 which claims
*	http://www.php.net/manual/en/function.strip-tags.php#96483
*	as its source. Werksferme.
*/
function strip_only( $str, $tags, $stripContent = FALSE, $limit = -1 ){
	$content = '';
	if(!is_array($tags) ){
		$tags = (strpos($str, '>') !== FALSE ? explode('>', str_replace('<', '', $tags)) : array($tags) );
		if( end($tags) == '' )
			array_pop( $tags );
	}
	
	foreach( $tags as $tag ){
		if( $stripContent ){
			$content = '(.+</'.$tag.'[^>]*>|)';
		}
		
		$str = preg_replace( '#</?'.$tag.'[^>]*>'.$content.'#is', '', $str, $limit );
	}
	
	return $str;
}

/**
*	return string
*/
function version(){
	$data = get_plugin_data( __DIR__.'/_plugin.php' );
	return $data['Version'];
}

/**
*
*	@param int
*	@param string
*	@return array
*/
function wp_post_to_tubmlr_params( $post_id, $type ){
	$post_body = get_post_field( 'post_content', $post_id );
	$post_excerpt = get_post_field( 'post_excerpt', $post_id );
	
	// Mimic wp_trim_excerpt() without The Loop.
	if( empty($post_excerpt) ){
		$text = $post_body;
		$text = strip_shortcodes($text );
		$text = apply_filters( 'the_content', $text );
		$text = str_replace( ']]>', ']]&gt;', $text );
		$text = wp_trim_words($text );
		$post_excerpt = $text;
	}

	$use_excerpt = FALSE; //get_tumblr_use_excerpt( $post_id ); // Use excerpt?
	$r = array();
	
	switch( $type ){
		case 'photo':
			$caption = $use_excerpt
				? apply_filters('the_excerpt', $post_excerpt)
				: apply_filters('the_content', strip_only($post_body, 'img', TRUE, 1) );

			$r['caption'] = strip_shortcodes( $caption );

			$r['link'] = extract_by_regex('/<img.*?src="(.*?)".*?\/?>/', $post_body, 1 );
			$r['source'] = extract_by_regex('/<img.*?src="(.*?)".*?\/?>/', $post_body, 1 );

			$photos = get_attached_media( 'image', $post_id );
			$data = array_map( __NAMESPACE__.'\get_file_paths', array_values($photos) );
			if( count($data) )
				$r['data'] = $data;

			break;

		case 'quote':
			$pattern = '/<blockquote.*?>(.*?)<\/blockquote>/s';
			$r['quote'] = wpautop(extract_by_regex($pattern, $post_body, 1) );
			$len = strlen(extract_by_regex($pattern, $post_body, 0) );

			$r['source'] = $use_excerpt
				? apply_filters('the_excerpt', $post_excerpt)
				: apply_filters('the_content', substr($post_body, $len) );
			break;

		case 'link':
			$r['title'] = get_post_field('post_title', $post_id);
			$r['url'] = ($use_excerpt && preg_match('/<a.*?href="(.*?)".*?>/', $post_excerpt))
				? extract_by_regex('/<a.*?href="(.*?)".*?>/', $post_excerpt, 1)
				: extract_by_regex('/<a.*?href="(.*?)".*?>/', $post_body, 1 );
			$r['description'] = $use_excerpt
				? apply_filters('the_excerpt', $post_excerpt)
				: apply_filters('the_content', $post_body);
			break;

		case 'chat':
			$r['title'] = get_post_field('post_title', $post_id);
			$r['conversation'] = wp_strip_all_tags($post_body);
			break;

		case 'audio':
			$caption = $use_excerpt
				? apply_filters('the_excerpt', $post_excerpt)
				: apply_filters('the_content', $post_body);

			$r['caption'] = strip_shortcodes( $caption );

			// Strip after apply_filters in case shortcode is used to generate <audio> element.
			$r['caption'] = strip_only( $r['caption'], 'audio', 1 );
			$r['external_url'] = extract_by_regex( '/(?:href|src)="(.*?\.(?:mp3|wav|wma|aiff|ogg|ra|ram|rm|mid|alac|flac))".*?>/i', $post_body, 1 );
			break;

		case 'video':
			$caption = $use_excerpt
				? apply_filters('the_excerpt', $post_excerpt)
				: apply_filters('the_content', strip_only($post_body, 'iframe', TRUE, 1) );

			$r['caption'] = strip_shortcodes( $caption );

			$pattern_youtube = '/youtube(?:-nocookie)\.com\/(?:v|embed)\/([\w\-]+)/';
			$pattern_vimeo = '/player\.vimeo\.com\/video\/([0-9]+)/';
				
			if( preg_match($pattern_youtube, $post_body) ){
				$r['embed'] = 'https://www.youtube.com/watch?v='
					. extract_by_regex($pattern_youtube, $post_body, 1 );
			} else if( preg_match($pattern_vimeo, $post_body) ){
				$r['embed'] = '<iframe src="//' . extract_by_regex($pattern_vimeo, $post_body, 0) . '">'
					. '<a href="//vimeo.com/' . extract_by_regex($pattern_vimeo, $post_body, 1). '">'
					. esc_html__('Watch this video.', 'tumblr-crosspostr') . '</a></iframe>';
			} else {
				// Pass along the entirety of any unrecognized <iframe>.
				$r['embed'] = extract_by_regex('/<iframe.*?<\/iframe>/', $post_body, 0);
			}

			$videos = get_attached_media( 'video', $post_id );
			$data = array_map( __NAMESPACE__.'\get_file_paths', array_values($videos) );

			// only supports one video
			if( count($data) ){
				$r['data'] = $data[0];
				unset( $r['embed'] );
			}

			break;
		case 'text':
			$r['title'] = get_post_field('post_title', $post_id);
			// fall through
		case 'aside':
		default:
			$r['body'] = $use_excerpt
				? apply_filters('the_excerpt', $post_excerpt)
				: apply_filters('the_content', $post_body);
			break;
	}
	
	if( !current_user_can('unfiltered_html') ){
		$r['title'] = htmlspecialchars_decode( $r['title'] );
	}

	return $r;
}

/**
*
*	@param string
*	@return string
*/
function wp_post_format_to_tumblr_post_type( $format ){
	switch( $format ){
		case 'audio':
		case 'chat':
		case 'link':
		case 'quote':
		case 'video':
			$type = $format;
			break;

		case 'image':
		case 'gallery':
			$type = 'photo';
			break;
			
		case 'aside':
		case FALSE:
		default:
			$type = 'text';
			break;
	}
	return $type;
}

/**
*	array_map callback
*	
*/
function wp_postmeta_result( $r ){
	$r = unserialize( $r );

	try{
		$r->api_data = get_tumblr( $r->tumblr_account )->getBlogPosts( $r->tumblr_blog, array('id' => $r->tumblr_id) );
	} catch( \Tumblr\API\RequestException $e ){
		// @TODO clean this up in postmeta-export.php
		$r->api_data = (object) array(
			'posts' => array(
				0 => (object) array(
					'post_url' => '',
					'error' => $e->getMessage()
				)
			)
		);
	}
	
	return $r;
}

/**
*	translates a WordPress post status to a Tumblr post state.
*	@param string $status The WordPress post status to translate.
*	@return mixed The translates Tumblr post state or FALSE if the WordPress status has no equivalently compatible state on Tumblr.
*/
function wp_status_to_tumblr_state( $status ){
	switch( $status ){
		case 'draft':
		case 'private':
			$state = $status;
			break;

		case 'future':
			// $state = 'queue';
			$state = 'private';
			break;

		case 'publish':
			$state = 'published';
			break;
		
		case 'auto-draft':
		case 'inherit':
		case 'pending':
		default:
			$state = FALSE;
	}
	
	return $state;
}