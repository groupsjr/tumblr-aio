<?php 

namespace sjr\tumblr_aio;

/*
*	register the import and export meta boxes
*/
function meta_box_add(){
	add_meta_box(
		'sjr-tumblr-import',
		'Tumblr Import',
		__NAMESPACE__.'\meta_box_import',
		'post'
	);

	add_meta_box(
		'sjr-tumblr-export',
		'Tumblr Export',
		__NAMESPACE__.'\meta_box_export',
		'post'
	);

	wp_register_script( 'sjr-tumblr-post', plugins_url('public/admin/post.js', __DIR__) );
	wp_enqueue_script( 'sjr-tumblr-post' );

	wp_register_style( 'sjr-tumblr-post', plugins_url('public/admin/post.css', __DIR__) );
	wp_enqueue_style( 'sjr-tumblr-post' );
}
add_action( 'add_meta_boxes', __NAMESPACE__.'\meta_box_add' );

/*
*	renders the meta box for export info
*	@param WP_Post
*	@param array
*	@return
*/
function meta_box_export( $post, $args ){
	$settings = get_setting( 'sjr-tumblr-settings' );
	$post_meta = get_post_meta( $post->ID );

	$tumblr_blogs = get_tumblr_blogs();
	$tumblr_posts = array();

	foreach( $tumblr_blogs as $tumblr_blog ){
		foreach( $tumblr_blog->user->blogs as $blog ){
			$meta_key = sprintf( '_tumblr_post_export[%s]', $blog->name );
			if( isset($post_meta[$meta_key]) ){
				$tumblr_posts = array_merge( $tumblr_posts, $post_meta[$meta_key] );
			}
		}
	}

	$tumblr_posts = array_map( __NAMESPACE__.'\wp_postmeta_result', $tumblr_posts );

	$vars = array(
		'selected' => $settings['default'],
		'tumblr_blogs' => $tumblr_blogs,
		'tumblr_posts' => $tumblr_posts
	);

	/*
	$tumblr_id = get_post_meta( $post->ID, '_tumblr_post_id', TRUE );

	if( $tumblr_id ){
		$vars->api_data = get_tumblr( 'devsjr' )->getBlogPosts( 'txch-merge-dev', array('id' => $tumblr_id) );
		$vars->tumblr_id = $tumblr_id;
	}
	*/

	echo render( 'admin/postmeta-export', $vars );
}

/*
*	renders the meta box for import info
*	@param WP_Post
*	@param array
*	@return
*/
function meta_box_import( $post, $args ){
	$tumblr_id = get_post_meta( $post->ID, 'tumblr_generalelectric_id', TRUE );
	$tumblr = get_tumblr( 'generalelectric' );

	$vars = array(
		'api_data' => $tumblr ? $tumblr->getBlogPosts( 'generalelectric', array('id' => $tumblr_id) ) : FALSE,
		'tumblr_id' => $tumblr_id
	);

	echo render( 'admin/postmeta-import', $vars );
}

/*
*	delete an exported post from the destination tumblr
*	@param int
*	@param int
*	@return
*/
function post_delete( $post_id, $tumblr_id ){
	$tumblr_posts = array();
	$post_meta = get_post_meta( $post_id );

	foreach( $post_meta as $key => $value ){
		if( strpos($key, '_tumblr_post_export[') === 0 ){
			$value = array_map( 'unserialize', $value );
			$tumblr_posts = array_merge( $tumblr_posts, $value );
		}
	}

	foreach( $tumblr_posts as $tumblr_post ){
		if( $tumblr_post->tumblr_id == $tumblr_id ){

			$tumblr = get_tumblr( $tumblr_post->tumblr_account );
			try{
				$res = $tumblr->deletePost( $tumblr_post->tumblr_blog, $tumblr_id, '' );

				if( $res->id ){
					$meta_key = sprintf( '_tumblr_post_export[%s]', $tumblr_post->tumblr_blog );

					delete_post_meta( $post_id, $meta_key, $tumblr_post );
				}
			} catch( \Tumblr\API\RequestException $e ){
				/* 
				$e->getCode() 
				$e->getMessage()
				*/
			}
		}
	}
}

/*
*
*	@param int
*	@param string
*	@return
*/
function post_export( $post_id, $tumblr_blog ){
	$tumblr_account = get_tumblr_account_name( $tumblr_blog );
	$tumblr = get_tumblr( $tumblr_account );

	$prepared_post = prepare_for_tumblr( $post_id );

	$post_meta = (object) array(
		'tumblr_account' => $tumblr_account,
		'tumblr_blog' => $tumblr_blog
	);

	try{
		if( !empty($prepared_post['id']) ){
			$res = $tumblr->editPost( $tumblr_blog, $prepared_post );
		} else {
			$res = $tumblr->createPost( $tumblr_blog, $prepared_post );
		}

		$post_meta->success = TRUE;
		$post_meta->tumblr_id = $res->id;

	} catch( \Tumblr\API\RequestException $e ){
		$post_meta->success = FALSE;
		$post_meta->error = $e;
	}

	add_post_meta( $post_id, "_tumblr_post_export[$tumblr_blog]", $post_meta );
}

/*
*	detects if any action should be taken on post save and routes to proper function
*	@param int
*	@param WP_Post
*	@param bool
*	@return
*/
function save_post( $post_id, $post, $update ){
	if( !\sjr\is_post_real_save($post) )
		return $post_id;

	// delete exported post
	if( !empty($_POST['sjr-tumblr-delete-export']) ){
		$delete = array_values( $_POST['sjr-tumblr-delete-export'] );
		$delete = array_pop( $delete );

		$nonce = array_keys( $_POST['sjr-tumblr-delete-export'] );
		$nonce = array_pop( $nonce );

		// @TODO verify nonce
		return post_delete( $post_id, $delete );
	}

	// delete imported post
	if( !empty($_POST['sjr-tumblr-delete-import']) ){

	}

	// re-import tumblr post
	if( !empty($_POST['sjr-tumblr-reimport']) && wp_verify_nonce($_POST['sjr-tumblr-reimport'], 'sjr-tumblr-reimport') ){
		
	}

	// publish / update post
	if( !empty($_POST['sjr-tumblr-do-export']) && !empty($_POST['sjr-export-blog']) ){
		return post_export( $post_id, $_POST['sjr-export-blog'] );
	}
}
add_action( 'save_post', __NAMESPACE__.'\save_post', 10, 3 );