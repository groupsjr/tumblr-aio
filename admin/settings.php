<?php 

namespace sjr\tumblr_aio;

/*
*
*	@param string
*	@return
*/
function admin_menu_controller( $active_tab = '' ){

	$vars = (object) array(
		'version' => version()
	);

	switch( $active_tab ){
		case 'accounts':
			$connected = isset( $_GET['sjr-tumblr-connect'] ) ? connect_tumblr_do() : FALSE;

			$vars->active_tab = 'accounts';
			$vars->form = admin_menu_accounts( $connected );
			break;

		case 'redirects':
			$vars->active_tab = 'redirects';
			$vars->form = admin_menu_redirects();
			break;

		case 'settings':
		default:
			$vars->active_tab = 'settings';
			$vars->form = admin_menu_settings();

			break;
	}

	echo render( 'admin/admin', $vars );
}

/*
*	render accounts tab /wp-admin/admin.php?page=sjr-tumblr-accounts
*	@param string
*/
function admin_menu_accounts( $connected = FALSE ){
	$accounts_blogs = get_tumblr_blogs();

	foreach( $accounts_blogs as &$accounts_blog ){
		$accounts_blog->remove_link = admin_url( sprintf('admin.php?page=sjr-tumblr-accounts&remove-account=%s&_wpnonce=%s', $accounts_blog->user->name, wp_create_nonce('remove-'.$accounts_blog->user->name)) );
	}

	$vars = array(
		'accounts_blogs' => $accounts_blogs,
		'connected' => $connected
	);

	return render( 'admin/admin-accounts', $vars );
}

/*
*	render redirects tab /wp-admin/admin.php?page=sjr-tumblr-redirects
*/
function admin_menu_redirects(){
	global $wpdb;

	$sql = "SELECT P.ID, PM.meta_value AS `original`, PM2.meta_value AS `new`
			FROM $wpdb->posts P
			LEFT JOIN $wpdb->postmeta PM
				ON P.ID = PM.post_id 
				AND PM.meta_key = 'tumblr_generalelectric_id'
			LEFT JOIN $wpdb->postmeta PM2
				ON P.ID = PM2.post_id 
				AND PM2.meta_key = '_tumblr_post_id'
			WHERE P.post_status = 'publish'
				AND PM.meta_value IS NOT NULL
				AND PM2.meta_value IS NOT NULL
			ORDER BY P.post_date DESC";

	$res = $wpdb->get_results( $sql );

	$vars = array(
		'posts' => $res
	);

	return render( 'admin/admin-redirects', $vars );
}

/*
*	render settings tab /wp-admin/admin.php?page=sjr-tumblr-settings
*/
function admin_menu_settings(){
	return render( 'admin/admin-settings' );
}

/*
*	save settings
*/
function admin_save(){
	// connect new account
	if( isset($_POST['_wpnonce']) && wp_verify_nonce($_POST['_wpnonce'], 'sjr-tumblr-account') ){
		return connect_tumblr( $_POST['sjr-tumblr-new-oauthconsumer'], $_POST['sjr-tumblr-new-secretkey'] );
	}

	// remove account
	if( isset($_GET['remove-account']) && wp_verify_nonce($_GET['_wpnonce'], 'remove-'.$_GET['remove-account']) ){
		
	}
}
add_action( 'admin_init', __NAMESPACE__.'\admin_save' );

/*
*   regsiter settings sections and fields for 'Settings' tab
*/ 
function settings_initialize(){
	add_settings_section(
        'sjr-tumblr-settings',                          // section id
        'Settings',                                     // header title
         __NAMESPACE__.'\settings_default_title',    // Callback used to render the description of the section
        'sjr-tumblr-settings'
    );

    add_settings_field(
        'sjr-tumblr-settings-default',
        'Default Blog',
         __NAMESPACE__.'\settings_default_field',    		 // render ui
        'sjr-tumblr-settings',                               // id for settings_fields
        'sjr-tumblr-settings'                          	 // parent id
    );

    add_settings_field(
        'sjr-tumblr-settings-taxonomy',
        'Tags Taxonomy',
         __NAMESPACE__.'\settings_taxonomy_field',    		 // render ui
        'sjr-tumblr-settings',                               // id for settings_fields
        'sjr-tumblr-settings'                          	 // parent id
    );

    return register_setting(
        'sjr-tumblr-settings',					// group
        'sjr-tumblr-settings',					// name
        __NAMESPACE__.'\settings_sanitize'               // validation callback
    );
}
add_action( 'admin_init', __NAMESPACE__.'\settings_initialize' );

/*
*	render UI for default blog selection
*/
function settings_default_field(){
	$settings = get_setting( 'sjr-tumblr-settings' );

	$vars = array(
		'selected' => $settings['default'],
		'tumblr_blogs' => get_tumblr_blogs(),
	);
	echo render( 'admin/admin-settings-default', $vars );
}

/*
*	render description for Settings section
*/
function settings_default_title(){
    echo '<p>lorem ipsum</p>';
}

/*
*	render UI for taxonomy selection
*/
function settings_taxonomy_field(){
	$settings = get_setting( 'sjr-tumblr-settings' );
	$vars = array(
		'taxonomy' => $settings['taxonomy']
	);

	echo render( 'admin/admin-settings-taxonomy', $vars );
}

/*
*
*/
function settings_sanitize( $values ){

	return $values;
}
