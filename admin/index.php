<?php 

namespace sjr\tumblr_aio;

require __DIR__.'/post.php';
require __DIR__.'/settings.php';

/*
*
*/
function admin_menu_add(){
	add_menu_page(
        'SJR Tumblr AIO',
        'Tumblr AIO',
        'manage-options',
        'sjr-tumblr-admin',
         ''
    );

	add_submenu_page(
		'sjr-tumblr-admin',
		'Tumblr AIO Accounts',
		'Accounts',
		'manage_options',
		'sjr-tumblr-accounts',
		function(){
			admin_menu_controller( 'accounts' );
		}
	);

	add_submenu_page(
		'sjr-tumblr-admin',
		'Tumblr AIO Redirects',
		'Redirects',
		'manage_options',
		'sjr-tumblr-redirects',
		function(){
			admin_menu_controller( 'redirects' );
		}
	);

	add_submenu_page(
		'sjr-tumblr-admin',
		'Tumblr AIO Settings',
		'Settings',
		'manage_options',
		'sjr-tumblr-settings',
		function(){
			admin_menu_controller( 'settings' );
		}
	);
}
add_action( 'admin_menu', __NAMESPACE__.'\admin_menu_add' );

