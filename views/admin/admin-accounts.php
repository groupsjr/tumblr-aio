<?php wp_nonce_field( 'sjr-tumblr-account', '_wpnonce' ); ?>

<?php if( $connected ): ?>
	<h2>Connected to <?php echo $connected; ?>!</h2>
<?php endif; ?>

<?php foreach( $accounts_blogs as $account ): ?>
	<ul>
		<li><h3><?php echo $account->user->name; ?></h3></li>
		<li>Blogs: <?php foreach( $account->user->blogs as $blog ): ?>
			<a href="<?php echo $blog->url; ?>" target="_blank"><?php echo $blog->title; ?> - <?php echo $blog->name; ?></a>
		<?php endforeach; ?></li>

		<li><a href="<?php echo $account->remove_link; ?>">remove</a></li>
	
	</ul>
<?php endforeach; ?>


<label>OAuth Consumer Key:</label> <input type="text" name="sjr-tumblr-new-oauthconsumer"/>
<label>Secret Key:</label> <input type="text" name="sjr-tumblr-new-secretkey"/>

<?php submit_button( 'Connect New' ); ?>