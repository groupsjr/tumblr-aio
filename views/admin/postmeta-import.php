<ul>
	<li>Tumblr ID: <?php echo $tumblr_id; ?> <a href="#">edit ID</a></li>

	<?php if( $api_data ): ?>
	<li>
		<a href="<?php echo $api_data->posts[0]->post_url; ?>" target="_blank">View On Tumblr</a>
		<a href="https://www.tumblr.com/edit/<?php echo $tumblr_id;?>" target="_blank">Edit in Tumblr</a>
		<button name="sjr-tumblr-delete-import[<?php echo wp_create_nonce( 'sjr-tumblr-delete-import'.$tumblr_post->tumblr_id ); ?>]" value="<?php echo $tumblr_post->tumblr_id; ?>" type="submit">Delete from Tumblr</button>
	</li>
	<?php endif; ?>

	<?php if( $api_data ): ?>
	<li>API Data:
		<textarea class="code"><?php echo json_encode( $api_data->posts[0], JSON_PRETTY_PRINT ); ?></textarea>
	</li>
	<?php endif; ?>
		
	<li><button type="submit" name="sjr-tumblr-reimport" value="<?php echo wp_create_nonce( 'sjr-tumblr-reimport' ); ?>">Reimport</button>
</ul>