<?php foreach( $tumblr_posts as $tumblr_post ): ?>
<ul>
	<?php if( $tumblr_post->success == TRUE ): ?>
	<li><h3><?php echo $tumblr_post->tumblr_blog; ?></h3></li>
	<li>Tumblr ID: <?php echo $tumblr_post->tumblr_id; ?> <a href="#" target="_blank">edit ID</a></li>
	<li>
		<?php if( $tumblr_post->api_data->posts[0]->post_url ): ?>
		<a href="<?php echo $tumblr_post->api_data->posts[0]->post_url; ?>" target="_blank">View On Tumblr</a>
		<?php endif; ?>
		<a href="https://www.tumblr.com/edit/<?php echo $tumblr_post->tumblr_id;?>" target="_blank">Edit in Tumblr</a>
		<button name="sjr-tumblr-delete-export[<?php echo wp_create_nonce( 'sjr-tumblr-delete-export'.$tumblr_post->tumblr_id ); ?>]" value="<?php echo $tumblr_post->tumblr_id; ?>" type="submit">Delete from Tumblr</button>
	</li>
	<?php endif; ?>

	<?php if( !empty($tumblr_post->api_data) ): ?>
	<li>API Data: <textarea class="code <?php echo empty($tumblr_post->api_data->posts[0]->error) ? '' : 'error'; ?>"><?php echo json_encode( $tumblr_post->api_data->posts[0], JSON_PRETTY_PRINT ); ?></textarea></li>
	<?php endif; ?>
</ul>
<?php endforeach; ?>

<ul>
	<li>Publish this post on Tumblr?
		<label>
			<input type="radio" name="sjr-tumblr-do-export" value="1"/>
			Yes
		</label>

		<label>
			<input type="radio" name="sjr-tumblr-do-export" value="0"/>
			No
		</label>
	</li>

	<li>Export to:
		<select name="sjr-export-blog">
			<?php foreach( $tumblr_blogs as $account ): ?>
				<optgroup label="<?php echo $account->user->name; ?>">
				<?php foreach( $account->user->blogs as $blog ): ?>
					<option value="<?php echo $blog->name; ?>" <?php selected( $blog->name, $selected ); ?>><?php echo $blog->title; ?> - <?php echo $blog->url; ?></option>
				<?php endforeach; ?>
				</optgroup>
			<?php endforeach; ?>
		</select>
	</li>
</ul>