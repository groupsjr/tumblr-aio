<select name="sjr-tumblr-settings[default]">
	<?php foreach( $tumblr_blogs as $account ): ?>
		<optgroup label="<?php echo $account->user->name; ?>">
		<?php foreach( $account->user->blogs as $blog ): ?>
			<option value="<?php echo $blog->name; ?>" <?php selected( $blog->name, $selected ); ?>><?php echo $blog->title; ?> - <?php echo $blog->url; ?></option>
		<?php endforeach; ?>
		</optgroup>
	<?php endforeach; ?>
</select>